import {Component, OnInit} from '@angular/core';
import {User} from './models/User';
import {ConfigService} from './services/config.service';
import {MenuService} from './services/menu.service';
import {MenuData, VoiceMenu} from './models/MenuData';
import {AuthService} from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  container: string;
  constructor(configService: ConfigService, public menuService: MenuService, public authService: AuthService){
    configService.home$.subscribe(({container}) => {
      this.container = container;
    });
  }
  ngOnInit(): void {
  }


  logout(){
    this.authService.logout();
  }

}
