import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ObjectUtils} from './shared/utils/objectUtils';
import {AppRoutingModule} from './app.routing.module';
import {AuthInterpcetor} from './interceptors/auth.interpcetor';
import {CoreModule} from './core/core.module';
import {FeaturesModule} from './features/features.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CoreModule,
    FeaturesModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
      ObjectUtils,
      {provide: HTTP_INTERCEPTORS, useClass: AuthInterpcetor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
