export class ObjectUtils {
    lastObject: any;

    isFull(obj: any = {}) : boolean{
        return obj && this.isObject(obj) && !!Object.keys(obj).length;
    }

    isEmpty(obj: any = {}): boolean{
        return this.isObject(obj) && !this.isFull();
    }

    isObject(obj): boolean{
        this.lastObject = obj;
        return typeof obj === 'object' && obj !== null && obj.constructor === Object;
    }

}
