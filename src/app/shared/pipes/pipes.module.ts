import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FilterArrayPipe} from './filter-array.pipe';
import {ObjectKeyPipe} from './object-key.pipe';


const exportComponents = [
    FilterArrayPipe,
    ObjectKeyPipe
];
@NgModule({
  declarations: [...exportComponents],
  imports: [
    CommonModule
  ],
  exports: [...exportComponents]
})
export class PipesModule { }
