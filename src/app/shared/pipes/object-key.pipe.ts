import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectKey'
})
export class ObjectKeyPipe implements PipeTransform {

  transform(value: any, keyValue: boolean = false): any[] {
    return value && Object.keys(value) && Object.keys(value).length ?
        this.switchKeyValue(value, keyValue) :
        [];
  }

  switchKeyValue(value, keyValue){
    return keyValue ? this.calcReduce(value) : Object.keys(value);
  }

  calcReduce(values){
    return Object.keys(values).map(key => ({key, value: values[key]}));
  }

}
