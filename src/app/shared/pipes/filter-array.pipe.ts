import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterArray'
})
export class FilterArrayPipe implements PipeTransform {

  transform(value: any[], search: string = '', property?: string | string[]): any[] {
    let result = [];
    if(value && value.length){
      result = value.filter(el => {
        if(property !== undefined){
          return this.findObj(el, property, search);
        } else {
          return el.toLowerCase().trim().indexOf(search.toLowerCase().trim()) !== -1;
        }
      })
    }
    return result;
  }

  findObj(obj, property, search){
    return typeof  property === 'string' ?
      this.findSingleElementObj(obj, property, search) : this.findElementsObj(obj, property, search);
  }

  findSingleElementObj(obj, property, search){
    return obj[property] && obj[property].toLowerCase().trim().indexOf(search.toLowerCase().trim()) !== -1;
  }

  findElementsObj(obj, property, search){
    let control = false;
    property.forEach(el => control = this.findSingleElementObj(obj, el, search) ? true : control);
    return control;
  }
}
