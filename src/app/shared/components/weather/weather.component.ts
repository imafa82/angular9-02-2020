import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WeatherApi} from './WeatherApi';

@Component({
  selector: 'app-weather',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, OnChanges {
  weather: WeatherApi;
  @Input() city: string;
  @Input() test: string;
  constructor(private http: HttpClient, private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  testFunc() {
    console.log('test');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.city){
      this.http.get('http://api.weatherstack.com/current?access_key=3f06917a4b742dbbe5eda9b0880ea454&query=' + this.city).subscribe((res: WeatherApi) => {
        this.weather = res;
        this.cd.markForCheck();
      });
    }
  }

}
