import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-error-management',
  templateUrl: './error-management.component.html',
  styleUrls: ['./error-management.component.scss']
})
export class ErrorManagementComponent implements OnInit {
  @Input() control: FormControl;
  errors: {type: string, message: (() => string) | string}[];
  constructor() {
    this.errors = [
      {
        type: 'required',
        message: 'Il campo è obbligatorio'
      },
      {
        type: 'minlength',
        message: this.getMinLengthMessage.bind(this)
      }
    ];
  }

  getMinLengthMessage(){
    return `Mancano ancora ${this.control.errors.minlength.requiredLength - this.control.errors.minlength.actualLength} caratteri`;
  }

  getMessageError(message){
    return typeof message === 'string' ? message : message();
  }

  ngOnInit(): void {
  }

}
