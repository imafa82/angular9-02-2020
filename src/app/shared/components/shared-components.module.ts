import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WeatherComponent} from './weather/weather.component';
import {ElementListComponent} from './design/element-list/element-list.component';
import {CustomCardComponent} from './design/bootstrap/custom-card/custom-card.component';
import {ListIcoComponent} from './design/list-ico/list-ico.component';
import {InfoCustomComponent} from './design/info-custom/info-custom.component';
import { PrintListComponent } from './design/print-list/print-list.component';
import { ErrorManagementComponent } from './form/error-management/error-management.component';
import { ErrorMessageComponent } from './form/error-message/error-message.component';


const exportComponent = [
  WeatherComponent,
  ElementListComponent,
  CustomCardComponent,
  ListIcoComponent,
  InfoCustomComponent,
  PrintListComponent,
  ErrorManagementComponent
]
@NgModule({
  declarations: [...exportComponent, ErrorMessageComponent],
  imports: [
    CommonModule
  ],
  exports: [
      ...exportComponent
  ]
})
export class SharedComponentsModule { }
