import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IconCard} from './models/IconCard';

@Component({
  selector: 'app-custom-card',
  templateUrl: './custom-card.component.html',
  styleUrls: ['./custom-card.component.scss']
})
export class CustomCardComponent implements OnInit {
  @Input() title: string;
  @Input() ico: string | IconCard[];
  @Output() action: EventEmitter<IconCard> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  icoAction(ico: IconCard){
    this.action.emit(ico);
  }



}
