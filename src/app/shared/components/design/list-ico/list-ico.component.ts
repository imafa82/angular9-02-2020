import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IconCard} from '../bootstrap/custom-card/models/IconCard';

@Component({
  selector: 'app-list-ico',
  templateUrl: './list-ico.component.html',
  styleUrls: ['./list-ico.component.scss']
})
export class ListIcoComponent implements OnInit {
  @Input() listIco: string | IconCard[];
  @Output() action: EventEmitter<IconCard> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }


  getIcoList(){
    return typeof this.listIco === 'string' ?
        [{ico: this.listIco, action: ''}] :
        this.listIco;
  }

  icoAction(ico: IconCard){
    this.action.emit(ico);
  }
}
