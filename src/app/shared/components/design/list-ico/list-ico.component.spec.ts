import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListIcoComponent } from './list-ico.component';

describe('ListIcoComponent', () => {
  let component: ListIcoComponent;
  let fixture: ComponentFixture<ListIcoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListIcoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListIcoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
