import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-info-custom',
  templateUrl: './info-custom.component.html',
  styleUrls: ['./info-custom.component.scss']
})
export class InfoCustomComponent implements OnInit {
  @Input() data: any;
  constructor() { }

  ngOnInit(): void {
  }

}
