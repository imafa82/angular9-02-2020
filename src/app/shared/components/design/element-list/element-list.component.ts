import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-element-list',
  templateUrl: './element-list.component.html',
  styleUrls: ['./element-list.component.scss']
})
export class ElementListComponent implements OnInit {
  @Input() value: string;
  @Input() label: string;

  constructor() { }

  ngOnInit(): void {
  }

}
