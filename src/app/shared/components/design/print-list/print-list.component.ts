import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-print-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './print-list.component.html',
  styleUrls: ['./print-list.component.scss']
})
export class PrintListComponent implements OnInit {
  @Input() array: string[];
  constructor() {
    this.array = [];
  }

  ngOnInit(): void {
  }
  test(){
    console.log('proviamo');
  }
}
