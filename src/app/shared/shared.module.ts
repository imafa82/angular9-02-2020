import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PipesModule} from './pipes/pipes.module';
import {SharedComponentsModule} from './components/shared-components.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PipesModule,
    SharedComponentsModule
  ],
  exports: [
      PipesModule,
      SharedComponentsModule
  ]
})
export class SharedModule { }
