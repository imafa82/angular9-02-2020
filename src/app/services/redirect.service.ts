import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RedirectService {
  private readonly redirect: any;
  constructor(private router: Router) {
    this.redirect = {
      login: 'login',
      base: 'home'
    };
  }


  redirectTo(where: string){
    this.router.navigate([this.redirect[where]]);
  }

  getUrlTree(where: string){
    return this.router.createUrlTree([this.redirect[where]]);
  }
}
