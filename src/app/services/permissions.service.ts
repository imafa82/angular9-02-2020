import { Injectable } from '@angular/core';
import {RedirectService} from './redirect.service';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {
  permissions: string[];
  changePermissions: Subject<string[]> = new Subject();
  constructor(private redirectService: RedirectService) {
    this.permissions = [];
  }

  setPermissions(permissions){
    this.permissions = permissions.map(perm => perm.permission);
    this.changePermissions.next(this.permissions);
  }

  issetPermissions(perm: string | string[]){
    return typeof perm === 'string' ? this.issetPermission(perm) : this.issetPermissionArray(perm);
  }


  private issetPermissionArray(permissions: string[] = []){

    return permissions.reduce((control, ele) => {
      if(!this.issetPermissions(ele)){
        control = false;
      }
      return control;
    }, true);
  }

  issetPermission(perm: string){
    return this.permissions.indexOf(perm) !== -1;
  }


  issetPermissionsWithRedirect(perm: string | string[]){
    if(!this.issetPermissions(perm)){
      this.redirectService.redirectTo('base');
    }
  }
}
