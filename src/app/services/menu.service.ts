import { Injectable } from '@angular/core';
import {MenuData, VoiceMenu} from '../models/MenuData';
import {AuthService} from './auth.service';
import {PermissionsService} from './permissions.service';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  menuData: MenuData;
  voices$: BehaviorSubject<VoiceMenu[]> = new BehaviorSubject([]);
  constructor(private authService: AuthService, private permissionService: PermissionsService) {
    this.permissionService.changePermissions.subscribe(permissions =>{
      this.voices$.next(this.getMenuData());
    });

    this.menuData = {
      voicesMenu: [
        {
          label: 'Home',
          view: 'home'
        },
        {
          label: 'Categorie',
          view: 'image-category'
        },
        {
          label: 'Lista spesa',
          view: 'shopping-list'
        },
        {
          label: 'Pipe',
          view: 'pipe-view'
        },
        {
          label: 'Forms',
          view: 'forms'
        },
        {
          label: 'Utenti Fake',
          view: 'users'
        },
        {
          label: 'Meteo',
          view: 'weather'
        },
        {
          label: 'Stile Dinamico',
          view: 'dynamic'
        },
        {
          label: 'Utenti',
          view: 'usersApp',
          perm: 'readUser'
        },
        {
          label: 'Attori',
          view: 'actors',
          perm: 'readActor'
        }
      ]
    };
  }

  getMenuData(): VoiceMenu[]{
    return this.authService.getUser() ? this.getMenuDataPermissions() : [];
  }

  getMenuDataPermissions(){
    return this.menuData.voicesMenu.filter(el => !el.perm ? true : this.permissionService.issetPermissions(el.perm));
  }
}
