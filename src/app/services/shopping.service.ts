import { Injectable } from '@angular/core';
import {ObjectUtils} from '../shared/utils/objectUtils';
@Injectable()


export class ShoppingService {
  list: string[];
  constructor(private objectUtils: ObjectUtils) {
    this.objectUtils.isObject({});
    this.init();

  }

  init(){
    this.list = ['frutta', 'verdura', 'patate'];
  }

  getList(){
    return [...this.list];
  }

  addElement(value): string[]{
    this.list.push(value);
    return this.getList();
  }

  removeElement(ele): string[]{
    //let index = this.list.indexOf(ele);
    //let index = this.list.findIndex(element => element === ele);
    //this.list.splice(index, 1);
    this.list = this.list.filter(element => element !== ele);
    return this.getList();
  }
}
