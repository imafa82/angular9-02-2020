import { TestBed } from '@angular/core/testing';

import { AuthLoadService } from './auth-load.service';

describe('AuthLoadService', () => {
  let service: AuthLoadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthLoadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
