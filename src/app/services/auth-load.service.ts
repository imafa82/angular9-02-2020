import { Injectable } from '@angular/core';
import {PermissionsService} from './permissions.service';
import {HttpClient} from '@angular/common/http';
import {UserLogged} from '../models/LoginResponse';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';
import {defer, of} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthLoadService {

  constructor(private permissionsService: PermissionsService, private http: HttpClient, private authService: AuthService) { }


  getPermissions(){
    return new Promise((resolve, reject) => {
      if(this.permissionsService.permissions && this.permissionsService.permissions.length){
        resolve(this.permissionsService.permissions);
      } else {
        this.http.get<UserLogged>(`${environment.urlBase}api/user`).subscribe(res => {
          this.authService.setUser(res);
          this.permissionsService.setPermissions(res.permissions);
          resolve(this.permissionsService.permissions);
        });
      }
    });
  }

  isLoggedWithCall(){
    return defer(() => !this.authService.getToken() ? of(this.authService.isLoggedWithRedirect()) : this.issetUserWithCall());

    // return new Promise((resolve, reject) => {
    //   if(!this.authService.getToken()){
    //     resolve(false);
    //   } else {
    //     if(this.authService.getUser()){
    //       resolve(true);
    //     } else {
    //       this.http.get<UserLogged>(`${environment.urlBase}api/user`).subscribe(res => {
    //         this.authService.setUser(res);
    //         resolve(true);
    //       });
    //     }
    //   }
    // });
  }

  issetUserWithCall(){
    return this.http.get<UserLogged>(`${environment.urlBase}api/user`).pipe(
        tap(res => this.authService.setUser(res)),
        switchMap(() => of(this.authService.isLoggedWithRedirect()))
    );
  }
}
