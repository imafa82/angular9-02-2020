import { Injectable } from '@angular/core';
import {UserLogged} from '../models/LoginResponse';
import {RedirectService} from './redirect.service';
import {PermissionsService} from './permissions.service';
import {UrlTree} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token: string;
  private user: UserLogged;
  constructor(private redirectService: RedirectService, private permissionsService: PermissionsService) {
    this.token = localStorage.getItem('token') || undefined;
  }

  issetUser(): boolean{
    return !!this.user;
  }
  setToken(token){
    this.token = token;
    if(token){
      localStorage.setItem('token', this.token);
    } else {
      localStorage.removeItem('token');
    }

  }

  logout(){
    this.setToken(undefined);
    this.user = undefined;
    this.permissionsService.setPermissions([]);
    this.redirectService.redirectTo('login');
  }

  getToken(){
    return this.token;
  }

  setUser(user){
    this.user = user;
    this.permissionsService.setPermissions(user.permissions);
  }

  getUser(){
    return this.user;
  }
  isLogged(): boolean{
    return !!this.token;
  }

  isLoggedWithRedirect(): boolean | UrlTree{
    // const isLogged: boolean = this.isLogged();
    // if(!isLogged){
    //   this.redirectService.redirectTo('login');
    // }
    return this.isLogged() || this.redirectService.getUrlTree('login');
  }
}
