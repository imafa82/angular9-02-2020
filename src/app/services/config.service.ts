import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  home$: BehaviorSubject<{container: string}> = new BehaviorSubject({container: 'container'});
  constructor() { }

  setContainer(bool: boolean = false){
    const container = bool ? 'container-fluid' : 'container';

    setTimeout(() => this.home$.next({...this.home$.getValue(), container}));
  }
}
