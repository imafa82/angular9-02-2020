import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild} from '@angular/router';
import { Observable } from 'rxjs';
import {PermissionsService} from '../services/permissions.service';
import {AuthLoadService} from '../services/auth-load.service';
import {RedirectService} from '../services/redirect.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivateChild {
  constructor(private permissionService: PermissionsService, private redirectService: RedirectService) { }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log(next);
    const permissions = next.data && next.data.permissions ? next.data.permissions : [];
    return this.permissionService.issetPermissions(permissions) || this.redirectService.getUrlTree('base');
  }

}
