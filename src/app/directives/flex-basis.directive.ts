import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appFlexBasis]'
})
export class FlexBasisDirective implements OnInit{
  @Input() appFlexBasis;
  @Input() col: number;
  constructor(private el: ElementRef, private  render: Renderer2) { }
  ngOnInit(): void {
    const totCols: number = this.appFlexBasis && this.appFlexBasis.cols ? this.appFlexBasis.cols : 12;
    const flexBasis= (this.col / totCols * 100) + '%';
    this.render.setStyle(this.el.nativeElement, 'flex-basis', flexBasis);
  }
}
