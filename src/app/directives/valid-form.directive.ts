import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {FormControl} from '@angular/forms';

@Directive({
  selector: '[appValidForm]'
})
export class ValidFormDirective implements OnInit{
  @Input() appValidForm: FormControl;
  constructor(private el: ElementRef, private renderer: Renderer2) { }
  ngOnInit(): void {
    this.renderer.addClass(this.el.nativeElement, 'form-control');
    this.appValidForm.statusChanges.subscribe(status => {
      if(status === 'VALID'){
        this.renderer.removeClass(this.el.nativeElement, 'is-invalid');
        this.renderer.addClass(this.el.nativeElement, 'is-valid');
      } else {
        this.renderer.removeClass(this.el.nativeElement, 'is-valid');
        this.renderer.addClass(this.el.nativeElement, 'is-invalid');
      }
    });
  }
}
