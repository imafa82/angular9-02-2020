import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appDisplayFlex]',
  exportAs: 'appDisplayFlex'
})
export class DisplayFlexDirective implements OnInit{
  @Input() set wrap (wrap){
      const flexWrap = wrap ? 'wrap': 'nowrap';
      this.setStyle({flexWrap});
      //this.render.setStyle(this.el.nativeElement, 'flex-wrap', wrap ? 'wrap' : 'nowrap');
  }

  @Input() set justify (justifyContent){
    if(justifyContent)
      this.setStyle({justifyContent});
  }

  @Input() set align (alignItems){
    if(alignItems)
      this.setStyle({alignItems});
  }



  _cols: number
  @Input() set cols(cols){
    this._cols = cols;
  }
  get cols(): number{
    return this._cols;
  }
  constructor(private el: ElementRef, private render: Renderer2) {
    this.cols = 12;
  }
  ngOnInit(): void {
    this.setStyle({display: 'flex'});
  }


  setStyle(obj){
    if(obj && Object.keys(obj) && Object.keys(obj).length){
      Object.keys(obj).forEach(el => {
        this.render.setStyle(this.el.nativeElement, this.setString(el), obj[el]);
      });
    }
  }

  setString(string: string){
    let s = '';
    for (let i=0; i< string.length; i++){
      let el = string[i];
      if(el.toUpperCase() === el){
        el = '-'+el.toLowerCase();
      }
      s = s + el;
    }
    return s;
  }
}
