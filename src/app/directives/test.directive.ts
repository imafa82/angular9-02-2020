import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appTest]'
})
export class TestDirective {
  constructor(private el: ElementRef, private render: Renderer2) {
    console.log(el);
    //el.nativeElement.style.backgroundColor = 'red';
    this.render.setStyle(el.nativeElement, 'background', 'red');
  }

  @HostListener('click') onClick(){
    this.render.setStyle(this.el.nativeElement, 'background', 'blue');
  }

}
