import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {PermissionsService} from '../services/permissions.service';

@Directive({
  selector: '[appPermission]'
})
export class PermissionDirective {
  private permissions: string | string[];
  private cond: boolean;
  @Input() set appPermission(val){
    this.permissions = val;
    this.checkPermission();
  }

  @Input() set appPermissionCond(cond){
    this.cond = !!cond;
    this.checkPermission();
  }
  constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef, private permissionService: PermissionsService) {
    this.cond = true;
  }

  checkPermission(){
    this.viewContainer.clear();
    if(this.permissionService.issetPermissions(this.permissions) && this.cond){
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}
