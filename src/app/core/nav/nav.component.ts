import {Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from '../../models/User';
import {VoiceMenu} from '../../models/MenuData';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss']
})
export class NavComponent {
    @Input() userLogged: User;
    @Input() voices: VoiceMenu[];
    @Output() logout: EventEmitter<void> = new EventEmitter();

    logoutAction(e){
        e.preventDefault();
        this.logout.emit();
    }
}
