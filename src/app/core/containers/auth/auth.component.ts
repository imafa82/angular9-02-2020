import { Component, OnInit } from '@angular/core';
import {UserLogged} from '../../../models/LoginResponse';
import {AuthService} from '../../../services/auth.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(public authService: AuthService, private http: HttpClient) { }

  ngOnInit(): void {

  }

}
