import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthComponent} from './containers/auth/auth.component';
import {RouterModule} from '@angular/router';
import {NavComponent} from './nav/nav.component';
import {FooterComponent} from './footer/footer.component';

const exportComp = [
  AuthComponent,
  NavComponent,
  FooterComponent
];

@NgModule({
  declarations: [
      ...exportComp,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
      ...exportComp
  ]
})
export class CoreModule { }
