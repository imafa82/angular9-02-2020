import { Component, OnInit } from '@angular/core';
import {interval, of} from 'rxjs';
import {filter, map, take} from 'rxjs/operators';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  date: number;
  constructor() {
    // interval(1000).subscribe(r => this.date = Date.now());
    // //of(2, 5, 6).subscribe(c => console.log(c * 5));
    // interval(1000).pipe(
    //     filter(el => (el % 2 === 0) && (el % 3 === 0)),
    //     map(el => el *3),
    //     take(5)
    // ).subscribe(
    //     r => console.log(r),
    //     err => console.log(err),
    //     () => console.log('completato')
    //     );
  }

  ngOnInit(): void {
  }

}
