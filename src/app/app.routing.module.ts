import { NgModule } from '@angular/core';

import { ShoppingListComponent } from './features/shopping-list/shopping-list.component';
import { ImageCategoryComponent } from './features/image-category/image-category.component';
import { HomeComponent } from './features/home/home.component';
import { PipeViewComponent } from './features/pipe-view/pipe-view.component';
import { WeatherPageComponent } from './features/weather-page/weather-page.component';
import { DynamicStyleComponent } from './features/dynamic-style/dynamic-style.component';
import {RouterModule} from '@angular/router';
import {ListUsersComponent} from './features/users/list-users/list-users.component';
import {UserDetailsComponent} from './features/users/user-details/user-details.component';
import {LoginComponent} from './features/login/login.component';
import {AuthGuard} from './guards/auth.guard';
import {AuthComponent} from './core/containers/auth/auth.component';
import {UsersAppComponent} from './features/users-app/users-app/users-app.component';
import {ActorsComponent} from './features/actors/actors.component';
import {ActorDetailComponent} from './features/actors/actor-detail/actor-detail.component';
import {ActorEditComponent} from './features/actors/actor-edit/actor-edit.component';
import {PermissionGuard} from './guards/permission.guard';
import {FormsReactiveComponent} from './features/forms-reactive/forms-reactive.component';

const routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: AuthComponent,
    canActivate: [AuthGuard],
    canActivateChild: [PermissionGuard],
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'dynamic',
        component: DynamicStyleComponent
      },
      {
        path: 'image-category',
        component: ImageCategoryComponent
      },
      {
        path: 'pipe-view',
        component: PipeViewComponent
      },
      {
        path: 'shopping-list',
        component: ShoppingListComponent
      },
        {
            path: '',
            loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule)
        },
      {
        path: 'usersApp',
        component: UsersAppComponent,
        data: {
          permissions: ['readUser']
        }
      },
      {
        path: 'forms',
        component: FormsReactiveComponent
      },
      {
        path: 'actors',
        component: ActorsComponent,
        data: {
          permissions: ['readActor']
        }
      },
      {
        path: 'actors/:id',
        component: ActorDetailComponent,
        data: {
          permissions: ['editActor']
        }
      },
      {
        path: 'weather',
        component: WeatherPageComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ]
})
export class AppRoutingModule { }
