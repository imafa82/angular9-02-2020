export interface MenuData {
    voicesMenu: VoiceMenu[];
}

export interface VoiceMenu {
    label:string;
    view: string;
    perm?: string | string[];
}
