export interface ObjectElement {
    key: string;
    value: any;
}
