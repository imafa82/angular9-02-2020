export interface FormInput {
    type: string;
    name: string;
    placeholder: string;
}
