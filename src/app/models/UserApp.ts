import {Permission} from './LoginResponse';

export interface UserApp {
    id: number;
    name: string;
    email: string;
    permissions: Permission[];
}
