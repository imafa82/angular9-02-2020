export interface LoginResponse {
    token: string;
    user: UserLogged;
}

export interface UserLogged {
    id: number;
    name: string;
    email: string;
    permissions: Permission[];
}

export interface Permission {
    id: number;
    permission: string;
    isset?: boolean;
}
