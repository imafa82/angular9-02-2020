import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {LoginService} from './login.service';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(f: NgForm){
    if(f.valid){
      this.loginService.login(f.value).subscribe(res => {
        if(res && res.token){
          this.authService.setToken(res.token);
          this.authService.setUser(res.user);
          this.router.navigate(['home']);
        }
        console.log(res);
      });
    }
  }

}
