import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginResponse} from '../../models/LoginResponse';
import {environment} from '../../../environments/environment';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }

  login(data): Observable<LoginResponse>{
    return this.http.post<LoginResponse>(`${environment.urlBase}api/auth/login`, data);
  }
}
