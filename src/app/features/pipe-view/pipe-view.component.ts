import { Component, OnInit } from '@angular/core';
import {ObjectUtils} from '../../shared/utils/objectUtils';

@Component({
  selector: 'app-pipe-view',
  template: `
    <p>Oggi è {{today | date: 'dd MMMM yyyy'}}</p>
    <p>Importo: {{import | currency: 'EUR'}}</p>
      <p>Numero: {{number | number: '2.2-2'}}</p>
      <pre>{{obj | json}}</pre>
  `,
  styleUrls: ['./pipe-view.component.scss'],
})
export class PipeViewComponent implements OnInit {
  today = Date.now();
  import = 1265768;
  number = 8.4564564;
  obj = {
    name: 'Massimiliano',
    surname: 'Salerno'
  };
  constructor(private objectUtils: ObjectUtils) {

  }

  ngOnInit(): void {
    console.log(this.objectUtils.lastObject);
    console.log(this.objectUtils.isObject({test: 'prova'}));
  }

}
