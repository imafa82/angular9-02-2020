import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-category',
  templateUrl: './image-category.component.html',
  styleUrls: ['./image-category.component.scss']
})
export class ImageCategoryComponent implements OnInit {
  categories: {category: string, label: string}[];
  selectedCategory: string;
  constructor() {
    setTimeout(()=> {
      this.categories = [
        {
          category: 'abstract',
          label: 'Astratti'
        },
        {
          category: 'animals',
          label: 'Animali'
        },
        {
          category: 'business',
          label: 'Business'
        },
        {
          category: 'cats',
          label: 'Gatti'
        },
        {
          category: 'city',
          label: 'Città'
        },
        {
          category: 'transport',
          label: 'Transporti'
        },
      ];
      this.selectedCategory = this.categories[0].category;
    }, 500);

  }

  setUrl(category){
    this.selectedCategory = category.category;
  }
  getUrl(){
    return 'https://loremflickr.com/320/240/' + this.selectedCategory;
  }

  ngOnInit(): void {
  }

}
