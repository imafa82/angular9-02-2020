import { NgModule } from '@angular/core';
import {ListUsersComponent} from './list-users/list-users.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {RouterModule} from '@angular/router';


const routes = [
  {
    path: 'users',
    component: ListUsersComponent
  },
  {
    path: 'users/:id',
    component: UserDetailsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
      RouterModule
  ]
})
export class UsersRoutingModule { }
