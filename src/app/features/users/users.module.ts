import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListUsersComponent} from './list-users/list-users.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {AddressInfoComponent} from './user-details/address-info/address-info.component';
import {GeneralInfoComponent} from './user-details/general-info/general-info.component';
import {ProfileInfoComponent} from './user-details/profile-info/profile-info.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {UsersRoutingModule} from './users.routing.module';



@NgModule({
  declarations: [
      ListUsersComponent,
      UserDetailsComponent,
      AddressInfoComponent,
      GeneralInfoComponent,
      ProfileInfoComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
