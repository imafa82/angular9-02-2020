import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserApi} from '../../../models/UserApi';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {
  users$: Observable<UserApi[]>;
  search: string;
  error: string;
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.users$ = this.http.get<UserApi[]>('https://jsonplaceholder.typicode.com/users').pipe(
        catchError(err => {
          console.log('test error');
          this.error = "Errore nel caricamento della lista degli utenti";
          return throwError(err);
        })
    );
  }
}
