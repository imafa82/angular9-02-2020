import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {UserApi} from '../../../models/UserApi';
import {IconCard} from '../../../shared/components/design/bootstrap/custom-card/models/IconCard';
import {HttpClient} from '@angular/common/http';
import {ConfigService} from '../../../services/config.service';
import {ActivatedRoute, Router} from '@angular/router';
import {forkJoin, of, throwError} from 'rxjs';
import {catchError, retry, switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, OnDestroy {
  user: Partial<UserApi>;
  listIcon: IconCard[];
  listFunc: any;
  constructor(
      private http: HttpClient,
      private configService: ConfigService,
      private router: Router,
      private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe((params: any) => this.init(params.id));
    this.listFunc = {
      resetSelectedUser: this.resetSelectedUser.bind(this),
      editUser: this.editUser.bind(this),
      nextUser: this.nextUser.bind(this)
    };
    this.listIcon = [
      {
        ico: 'arrow-circle-left',
        action: 'resetSelectedUser'
      },
      {
        ico: 'pencil',
        action: 'editUser'
      },
      {
        ico: 'arrow-circle-right',
        action: 'nextUser'
      }
    ];
  }

  ngOnInit(): void {
    this.configService.setContainer(true);
  }

  init(id){
    const users$ = this.http.get<UserApi>('https://jsonplaceholder.typicode.com/users/' + id);
    const posts$ = this.http.get<any>(`https://jsonplaceholder.typicode.com/users/${id}/posts`).pipe(
        catchError(err => {
          console.log('array vuoto perchè dà errore');
          return of([]);
        })
    );
    users$.pipe(
        tap(user => this.user = user),
        switchMap(res => posts$)
    ).subscribe(res => {
      console.log(res);
    });
    // forkJoin([users$, posts$]).pipe(
    //   retry(2),
    //   catchError(err => {
    //     console.log('aprire un popup con messaggio di errore');
    //     return throwError(err);
    //   })
    // ).subscribe(([users, posts]) => {
    //   this.user = users;
    //   console.log(posts);
    // }, err => console.log(err));
  }
  ngOnDestroy(): void {
    this.configService.setContainer();
  }

  nextUser(){
    let id = + this.activatedRoute.snapshot.params.id + 1;
    console.log(id);
    this.router.navigate(['users', id]);
  }

  resetSelectedUser(){
    this.router.navigate(['users']);
  }

  actionIco(ico){
    ico.action && this.listFunc && this.listFunc[ico.action] && this.listFunc[ico.action]();
  }

  editUser(){
    console.log('questo attiverà la modalità di modifica');
  }


  // setUser(user){
  //   this.selectedUser = user;
  //   this.configService.setContainer(true);
  // }

}
