import {Component, Input, OnInit} from '@angular/core';
import {UserApi} from '../../../../models/UserApi';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.scss']
})
export class ProfileInfoComponent implements OnInit {
  @Input() user: UserApi;
  constructor() { }

  ngOnInit(): void {
  }

}
