import {Component, Input, OnInit} from '@angular/core';
import {UserAddress} from '../../../../models/UserApi';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html',
  styleUrls: ['./address-info.component.scss']
})
export class AddressInfoComponent implements OnInit {
  @Input() address: UserAddress;
  constructor(public sanitazer: DomSanitizer) { }

  ngOnInit(): void
  {
  }

  getUrl(){
    return this.sanitazer.bypassSecurityTrustResourceUrl(
        'https://www.openstreetmap.org/export/embed.html?marker=' + this.address.geo.lat + '%2C' + this.address.geo.lng
    );
  }

}
