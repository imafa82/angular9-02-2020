import {Component, OnDestroy, OnInit} from '@angular/core';
import {element} from 'protractor';
import {ShoppingService} from '../../services/shopping.service';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss'],
  providers: [ShoppingService]
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  openCard: boolean;
  list: string[];
  search: string;
  subscription: Subscription;
  constructor(private shoppingService: ShoppingService) {
    const observable = new Observable(subscriber => {
      subscriber.next('ciliegie');
      setTimeout(() =>   {
        subscriber.next('mele');
        subscriber.error('errore');
        subscriber.complete();
      }, 8000);
      setTimeout(() =>   subscriber.next('albicocche'), 2000);
    });

    this.subscription = observable.subscribe(
      r => {
              console.log('dentro');
              this.list = this.shoppingService.addElement(r);
            },
      err =>  {console.log(err)},
  () => {console.log('complete')}
    );
    this.search = '';
    this.openCard = true;
    this.list = this.shoppingService.getList();
  }

  removeElement(ele: string){
    this.list = this.shoppingService.removeElement(ele);
  }

  addElement(el){
    if(el.target.value !== ''){
      //this.list.push(el.target.value);
      this.list = this.shoppingService.addElement(el.target.value);
      el.target.value = '';
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
  }

}
