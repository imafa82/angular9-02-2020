import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ObjectElement} from '../../models/ObjectElement';

@Component({
  selector: 'app-dynamic-style',
  templateUrl: './dynamic-style.component.html',
  styleUrls: ['./dynamic-style.component.scss']
})
export class DynamicStyleComponent implements OnInit {
  stylePage: any;
  array: string[];
  constructor() {
    this.stylePage = {
      backgroundColor: 'red',
      width: '500px',
      height: '500px'
    };

    this.array = ['test', 'test1', 'test2', 'test3'];
    setTimeout(() => {
      this.array = [...this.array, 'test4'];
    }, 2000);
  }

  ngOnInit(): void {
  }

  getArrayStyle(){
    return Object.keys(this.stylePage);
  }

  removeStyle(el){
    const stylePage = Object.keys(this.stylePage).reduce((obj: any, key: string) => {
      if(key !== el){
        obj[key] = this.stylePage[key];
      }
      return obj;
    }, {});


    this.stylePage = stylePage;
  }

  setChangeStyle(element: ObjectElement){
    this.stylePage[element.key] = element.value;
    this.stylePage = {...this.stylePage};
  }

  addStyle(f: NgForm){
    console.log(f.value);
    const {property, value} = f.value;
    const el = {...this.stylePage};
    el[property] = value;
    this.stylePage = el;
    // const property = f.value.property;
    // const value = f.value.value;

    //this.stylePage = {...this.stylePage};
    f.reset();
    //this.stylePage[this.newStyle.property] = this.newStyle.value;
  }
}
