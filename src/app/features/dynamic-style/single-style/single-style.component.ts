import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ObjectElement} from '../../../models/ObjectElement';

@Component({
  selector: 'app-single-style',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './single-style.component.html',
  styleUrls: ['./single-style.component.scss']
})
export class SingleStyleComponent implements OnInit {
  @Input() element: ObjectElement;
  @Output() rm: EventEmitter<string> = new EventEmitter();
  @Output() ch: EventEmitter<ObjectElement> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  removeStyle(){
    this.rm.emit(this.element.key);
  }

  setChangeStyle(){
    this.ch.emit(this.element);
  }


}
