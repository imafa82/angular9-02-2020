import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleStyleComponent } from './single-style.component';

describe('SingleStyleComponent', () => {
  let component: SingleStyleComponent;
  let fixture: ComponentFixture<SingleStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
