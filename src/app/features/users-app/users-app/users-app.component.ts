import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {UserApp} from '../../../models/UserApp';
import {Permission} from '../../../models/LoginResponse';
import {FormArray, FormControl, FormGroup, NgForm} from '@angular/forms';
import {PermissionsService} from '../../../services/permissions.service';
import {exhaustMap, map, switchMap} from 'rxjs/operators';
import {fromEvent} from 'rxjs';

@Component({
  selector: 'app-users-app',
  templateUrl: './users-app.component.html',
  styleUrls: ['./users-app.component.scss']
})
export class UsersAppComponent implements OnInit, AfterViewInit {
  users: any[];
  selectedUser: any;
  wrap: boolean;
  permissions: string[];
  permissionsForm: FormGroup;
  @ViewChild('btnCall') btnCall: ElementRef<HTMLButtonElement>;
  constructor(private http: HttpClient, private permissionService:PermissionsService) {
    // if(!permissionService.issetPermissions('readUser')){
    //   console.log('redirect home');
    this.permissions = ['editPermission'];

    this.wrap = true;

    this.permissionService.issetPermissionsWithRedirect('readUser');
  }
  ngAfterViewInit(): void {
    fromEvent(this.btnCall.nativeElement, 'click').pipe(
        exhaustMap(() => this.http.get<any[]>(`${environment.urlBase}api/users`))
    ).subscribe(res => console.log(res));
  }

  ngOnInit(): void {
    this.http.get<any[]>(`${environment.urlBase}api/users`).subscribe(res => {
      console.log(res);
      this.users = res;
    });
  }

  callElement(){
    this.http.get<any[]>(`${environment.urlBase}api/users`).subscribe(res => {
      console.log(res);
    });
  }

  setUser(user){
    this.http.get<{user: UserApp, permissions: Permission[]}>(`${environment.urlBase}api/users/${user.id}`).pipe(
        map(res => {
          res.permissions = res.permissions.map(perm => {
            perm.isset = !!res.user.permissions.find(p => p.id === perm.id);
            return perm;
          });
          delete res.user.permissions;
          return res;
        })
    ).subscribe(res => {
      this.selectedUser = res;
      this.permissionsForm = new FormGroup( {
        permissions: new FormArray(this.setArrayPermissions(res.permissions))
      });
      console.log(this.selectedUser);
    });
  }

  setArrayPermissions(permissions){
    return permissions.reduce((perms, el) => {
      perms.push(new FormGroup(this.createFormGroup(el)));
      return perms;
    }, []);
  }

  createFormGroup(el){
    return Object.keys(el).reduce((forms, data) => {
      forms[data] = new FormControl(el[data]);
      return forms;
    }, {})
  }

  getPermissions(){
    return <FormArray> this.permissionsForm.get('permissions');
  }

  addPermissions(){

    // console.log(this.selectedUser.permissions);
    //const permissions = this.selectedUser.permissions.filter(el => el.isset).map(el => el.id);

    // const permissions = Object.keys(form.value).filter(el => form.value[el]);
    // console.log(permissions);

    const formPermissions = this.permissionsForm.get('permissions').value;
    const permissions = formPermissions.filter(el => el.isset).map(el => el.id);
    this.http.put<any>(`${environment.urlBase}api/users/${this.selectedUser.user.id}/permissions`,{permissions}).subscribe(res => {
      console.log(res);
    });
  }

}
