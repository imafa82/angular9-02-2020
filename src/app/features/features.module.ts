import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DynamicStyleComponent} from './dynamic-style/dynamic-style.component';
import {HomeComponent} from './home/home.component';
import {ImageCategoryComponent} from './image-category/image-category.component';
import {LoginComponent} from './login/login.component';
import {PipeViewComponent} from './pipe-view/pipe-view.component';
import {ShoppingListComponent} from './shopping-list/shopping-list.component';
import {WeatherPageComponent} from './weather-page/weather-page.component';
import {SingleStyleComponent} from './dynamic-style/single-style/single-style.component';
import {SharedModule} from '../shared/shared.module';
import { UsersAppComponent } from './users-app/users-app/users-app.component';
import {TestDirective} from '../directives/test.directive';
import {DisplayFlexDirective} from '../directives/display-flex.directive';
import {FlexBasisDirective} from '../directives/flex-basis.directive';
import {PermissionDirective} from '../directives/permission.directive';
import { ActorsComponent } from './actors/actors.component';
import { ActorDetailComponent } from './actors/actor-detail/actor-detail.component';
import { ActorEditComponent } from './actors/actor-edit/actor-edit.component';
import {ValidFormDirective} from '../directives/valid-form.directive';
import { FormsReactiveComponent } from './forms-reactive/forms-reactive.component';

const exportComp = [
    DynamicStyleComponent,
    HomeComponent,
    ImageCategoryComponent,
    LoginComponent,
    PipeViewComponent,
    ShoppingListComponent,
    WeatherPageComponent,
    ActorsComponent,
    ActorDetailComponent,
    ActorEditComponent,
    FormsReactiveComponent
];

@NgModule({
  declarations: [
      ...exportComp,
      SingleStyleComponent,
      UsersAppComponent,
      TestDirective,
      DisplayFlexDirective,
      FlexBasisDirective,
      PermissionDirective,
      ValidFormDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [
      ...exportComp
  ]
})
export class FeaturesModule { }
