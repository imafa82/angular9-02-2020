import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-forms-reactive',
  templateUrl: './forms-reactive.component.html',
  styleUrls: ['./forms-reactive.component.scss']
})
export class FormsReactiveComponent implements OnInit {
  myForm: FormGroup;
  constructor() {
    this.myForm = new FormGroup({
      name: new FormControl(''),
      surname: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
    });
    this.myForm.valueChanges.subscribe(res => {
      console.log(res);
    });
  }

  enableDisableSurname(){
    if(this.myForm.get('surname').enabled){
      this.myForm.get('surname').disable();
    }else {
      this.myForm.get('surname').enable();
    }
  }

  ngOnInit(): void {
  }

}
