import { Component, OnInit } from '@angular/core';
import {ObjectUtils} from '../../shared/utils/objectUtils';
import {PermissionsService} from '../../services/permissions.service';
import {BehaviorSubject, from, interval, ReplaySubject, Subject} from 'rxjs';
import {map, reduce, scan, share} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  title: string = 'La nostra prima App';
  gender: string = 'F';
  constructor(public objectUtils: ObjectUtils, private permissionService: PermissionsService) {

    // let behavior = new BehaviorSubject(6);
    // let subject = new Subject();
    // let replaySubject = new ReplaySubject();
    // behavior.next(5);
    // subject.next(5);
    // replaySubject.next(5);
    // replaySubject.next(7);
    // replaySubject.next(9);
    // replaySubject.next(12);
    // replaySubject.next(21);
    //
    // behavior.subscribe(res => console.log(res, 'behavior'));
    // subject.subscribe(res => console.log(res, 'subject'));
    // replaySubject.subscribe(res => console.log(res, 'replySub first'));
    //
    // setTimeout(()=> {
    //   behavior.next(89);
    //   subject.next(89);
    //   replaySubject.subscribe(res => console.log(res, 'replySub second'));
    //   setTimeout(() =>{
    //     replaySubject.next(6666542);
    //   }, 2000);
    // }, 5000);
    // let sub: Subject<number> = new Subject();
    //
    // let test$ = interval(1000).pipe(
    //     map(() => Math.random())
    // ).subscribe(sub);
    //
    //
    //
    // sub.subscribe(r => console.log(r, 1));
    // sub.subscribe(r => console.log(r, 2));
    //this.objectUtils = objectUtils;
    //this.objectUtils = new ObjectUtils();
    // let arr = [
    //   {
    //     name: 'Massimiliano',
    //     post: 5
    //   },
    //   {
    //     name: 'Fabrizio',
    //     post: 8
    //   },
    //   {
    //     name: 'Carlo',
    //     post: 2
    //   }
    // ];
    //console.log(arr.reduce((sum, el) => sum + el.post, 0));
    // from(arr).pipe(
    //     reduce((sum, el) => {
    //       return sum + el.post;
    //     }, 0)
    // ).subscribe(res => {
    //   console.log(res);
    // });
  }

  ngOnInit(): void {

  }
  setGender(){
    this.gender = this.gender === 'F' ? 'M' : 'F';
  }

  setTitle(event){
    this.title = event.target.value;
  }
  getTitle() {
    return this.title;
  }
}
