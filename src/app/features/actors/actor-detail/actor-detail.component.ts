import { Component, OnInit } from '@angular/core';
import {Actor} from '../models/Actor';
import {ActorsService} from '../actors.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-actor-detail',
  templateUrl: './actor-detail.component.html',
  styleUrls: ['./actor-detail.component.scss'],
  providers: [ActorsService]
})
export class ActorDetailComponent implements OnInit {
  actor: Actor;
  editActor: boolean;
  constructor(private actorService: ActorsService, actiatedRoute: ActivatedRoute) {
    actiatedRoute.params.subscribe((params: {id: number}) => {
     this.actorService.getActor(params.id).subscribe(res => this.actor = res);
    });
  }

  ngOnInit(): void {
  }

  editActorAction(actor: Actor){
    this.actorService.updateActor(actor).subscribe(res => {
      this.actor = {...this.actor, ...res},
      this.editActor = false;
    });
  }

}
