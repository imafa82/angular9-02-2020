import { Component, OnInit } from '@angular/core';
import {ActorsService} from './actors.service';
import {Actor} from './models/Actor';

@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.scss'],
  providers: [ActorsService]
})
export class ActorsComponent implements OnInit {
  actors: Actor[];
  constructor(private actorService: ActorsService) {
    this.actorService.getActors().subscribe(res => {
      this.actors = res;
    });
  }

  ngOnInit(): void {
  }

  addActor(actor: Partial<Actor>){
    this.actorService.createActor(actor).subscribe(res => {
      this.actors.push(res);
    });
  }

  deleteActor(event, id: number){
    event.stopPropagation();
    this.actorService.deleteActor(id).subscribe(res => {
     if(res)
       this.actors = this.actors.filter(actor => actor.id !== id);
    });
  }

}
