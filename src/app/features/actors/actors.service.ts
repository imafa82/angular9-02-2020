import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Actor} from './models/Actor';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class ActorsService {
  private readonly baseUrl: string;
  constructor(private http: HttpClient) {
    this.baseUrl = environment.urlBase + 'api/actors';
  }

  getActors(): Observable<Actor[]>{
    return this.http.get<{data: Actor[]}>(this.baseUrl).pipe(
        map(res => res.data )
    );
  }

  getActor(id: number): Observable<Actor>{
    return this.http.get<{data: Actor}>(`${this.baseUrl}/${id}`).pipe(
        map(res => res.data )
    );
  }

  createActor(data: any): Observable<Actor>{
    return this.http.post<{data: Actor}>(this.baseUrl, data).pipe(
        map(res => res.data )
    );
  }

  updateActor(data: Actor): Observable<Actor>{
    return this.http.patch<{data: Actor}>(`${this.baseUrl}/${data.id}`, data).pipe(
        map(res => res.data )
    );
  }

  deleteActor(id: number): Observable<boolean>{
    return this.http.delete<{data: boolean}>(`${this.baseUrl}/${id}`).pipe(
        map(res => res.data)
    );
  }
}
