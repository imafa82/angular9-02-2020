import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Actor} from '../models/Actor';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {FormInput} from '../../../models/FormInput';

@Component({
  selector: 'app-actor-edit',
  templateUrl: './actor-edit.component.html',
  styleUrls: ['./actor-edit.component.scss']
})
export class ActorEditComponent implements OnInit {
  @Input() actor: Actor;
  @Output() sub: EventEmitter<Partial<Actor>> = new EventEmitter();
  editForm: FormGroup;
  formInput: FormInput[];
  constructor() {
    this.formInput = [
      {
        type: 'text',
        name: 'name',
        placeholder: 'Nome Attore'
      },
      {
        type: 'number',
        name: 'year',
        placeholder: 'Anno di nascita'
      },
      {
        type: 'text',
        name: 'photo',
        placeholder: 'Url Immagine'
      }
    ];
  }

  ngOnInit(): void {

    let obj = {
      name: new FormControl(this.actor ? this.actor.name : '', [Validators.required, Validators.minLength(3)]),
      year: new FormControl(this.actor ? this.actor.year : '', [Validators.required]),
      photo: new FormControl(this.actor ? this.actor.photo : '')
    };
    let edit = this.actor ? {
      id: new FormControl(this.actor.id)
    } : {};
    this.editForm = new FormGroup({
      ...obj,
      ...edit
    });
  }
  confirm(){
    if(this.editForm.valid){
      this.sub.emit(this.editForm.value);
    }
  }

}
