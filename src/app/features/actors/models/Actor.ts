export interface Actor {
    id: number;
    name: string;
    photo: string;
    year: string;
    address?: ActorAddress;
    films: ActorFilm[];
}


export interface ActorAddress {
    id: number;
    name: string;
    lat: number;
    long: number;
}


export interface ActorFilm {
    id: number;
    title: string;
    year: number;
    description: string;
}
