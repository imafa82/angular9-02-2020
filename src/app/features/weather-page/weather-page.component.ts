import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import {fromEvent, MonoTypeOperatorFunction, OperatorFunction} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-weather-page',
  templateUrl: './weather-page.component.html',
  styleUrls: ['./weather-page.component.scss']
})
export class WeatherPageComponent implements OnInit {
  city: string;
  test: string;
  //@ViewChild('search') searchInput: ElementRef<HTMLInputElement>;
  search: FormControl;
  constructor() {
    this.city = 'Rome'
    this.search = new FormControl(this.city);
    this.search.valueChanges.pipe(
        this.customOperator(2)
    ).subscribe(res => {
      console.log(res);
      this.city = res;
    })
    // this.test = 'Test';
    // setTimeout(()=> {
    //   this.city = 'London';
    // }, 5000);
    // setTimeout(()=> {
    //   this.test = 'Dopo 2 secondi';
    // }, 2000);
  }

  ngOnInit(): void {
  }

  // ngAfterViewInit(): void {
  //   console.log(this.searchInput);
  //   // this.searchInput.nativeElement.addEventListener('click', function(e) {
  //   //   alert('click');
  //   // })
  //
  //   // fromEvent<KeyboardEvent>(this.searchInput.nativeElement, 'keyup').pipe(
  //   //   this.customOperator(2)
  //   // ).subscribe(res => {
  //   //   this.city = res;
  //   // });
  // }

  customOperator(len: number): MonoTypeOperatorFunction<string>{
    return input$ => input$.pipe(
        debounceTime(500),
        filter(text => text.length > len),
        distinctUntilChanged()
    );
  }


  setCity(form: NgForm){
    if(form.valid)
      this.city = form.value.city;
  }
}
