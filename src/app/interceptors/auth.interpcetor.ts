import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthInterpcetor implements HttpInterceptor{
    constructor(private authService: AuthService){}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(this.authService.isLogged() && req.url && req.url.startsWith(environment.urlBase)){
            const headers = new HttpHeaders().set('Authorization', 'bearer ' + this.authService.getToken());
            req = req.clone({headers});
        }
        return next.handle(req).pipe(
            catchError((err : any)=> {
                if(err instanceof HttpErrorResponse){
                    if(err.status === 401){
                        this.authService.logout();
                    }
                }
                return throwError(err);
            })
        );
    }
}
